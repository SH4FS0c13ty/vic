# Video Integrity Checker (VIC)

VIC is a Python script designed to check integrity of multiple video files from a folder at once.

## Requirements

- [Python 3.6+](https://www.python.org/downloads/) with [pip](https://pip.pypa.io/en/stable/) (Windows & Linux)

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install VIC python requirements:

```bash
python3 -m pip install -r requirements.txt
```


## Usage

```text
usage: vic.py ffmpeg_path [-h] [-q] [-t THREADS] [-ft FFMPEG_THREADS]
              [-o OUTPUT] [-d DIRECTORY [DIRECTORY ...]]
              [-x EXCLUDE [EXCLUDE ...]]

Check video files integrity from folder using ffmpeg.

positional arguments:
  ffmpeg_path           path to ffmpeg binary

optional arguments:
  -h, --help            show this help message and exit
  -q, --quiet           suppress output
  -t THREADS, --threads THREADS
                        number of threads to use (default: 5)
  -ft FFMPEG_THREADS, --ffmpeg_threads FFMPEG_THREADS
                        number of threads to use with ffmpeg (default: 2)
  -o OUTPUT, --output OUTPUT
                        write output to a specified file
  -d DIRECTORY [DIRECTORY ...], --directory DIRECTORY [DIRECTORY ...]
                        directory where video files are stored (default: current)
  -x EXCLUDE [EXCLUDE ...], --exclude EXCLUDE [EXCLUDE ...]
                        folders or files to exclude (absolute paths, regex allowed)
```


## Contributing

Pull requests are welcome. For major changes or bug discovery, please [open an issue](https://gitlab.com/SH4FS0c13ty/vic/issues) mentioning the name of the script in the title, to discuss changes.


## License

[MIT License](https://choosealicense.com/licenses/mit/)

```text
MIT License

Copyright (c) 2024 Adrien CL

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```
