#! /usr/bin/env python3
# -*- coding: utf-8 -*-
########################################################################
##                       Video Integrity Checker                      ##
########################################################################
##             VIC.py is a part of Video Integrity Checker            ##
########################################################################
## vic.py is used to verify integrity of video files using ffmpeg     ##
## binaries (that you have to provide). It uses ThreadPoolExecutor to ##
## process multiple files quicker than with a single thread.          ##
########################################################################
## Shell syntax: vic.py ffmpeg_path [-h] [-q] [-t THREADS]            ##
##                      [-ft FFMPEG_THREADS] [-o OUTPUT]              ##
##                      [-d DIRECTORY [DIRECTORY ...]]                ##
##                      [-f FILE [FILE ...]]                          ##
##                      [-x EXCLUDE [EXCLUDE ...]]                    ##
########################################################################
## Author: Adrien CL                                                  ##
## Copyright: Copyright 2024, Adrien CL                               ##
## Credits: [Adrien CL]                                               ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 2.0.0                                                     ##
## Repository: https://gitlab.com/SH4FS0c13ty/vic                     ##
## Maintainer: Adrien CL                                              ##
## Email: adrienclt@proton.me                                         ##
########################################################################

# Internal modules
import os, re
from argparse import ArgumentParser, ArgumentError
from colorama import Fore, Style, init as ColoramaInit
from concurrent.futures import ThreadPoolExecutor, as_completed as CompletedFutures, thread as FutureThread
from subprocess import Popen, PIPE
from threading import Lock


class CustomArgumentParser():
    """ Handle arguments """
    def __init__(self):
        return

    def _CheckFileExistence(self, Path:str):
        """ Check if path is a valid file """
        Path = os.path.abspath(Path).replace("\\", "/")
        if os.path.isfile(Path):
            return Path
        else:
            raise FileNotFoundError("The following file could not be found or is not a file: \"{}\"".format(Path))

    def _CheckFolderExistence(self, Path:str):
        """ Check if path is a valid folder """
        Path = os.path.abspath(Path).replace("\\", "/")
        if os.path.isdir(Path):
            return Path
        else:
            raise FileNotFoundError("The following folder could not be found or is not a folder: \"{}\"".format(Path))

    def _CheckFileWriteAccess(self, Path:str):
        """ Check if parent path is a valid folder """
        Path = os.path.abspath(Path).replace("\\", "/")
        if os.path.isdir(Path.rsplit("/", 1)[0]):
            with open(Path, "a"):
                return Path
        else:
            raise NotADirectoryError("The following parent folder could not be found: \"{}\"".format(Path.rsplit("/", 1)[0]))

    def _CheckPositiveInteger(self, Number:int):
        """ Check if the submitted number is a positive integer """
        try:
            Number = int(Number)
            if Number > 0:
                return Number
            else:
                raise ValueError
        except ValueError:
            raise ValueError("The following value is not and cannot be converted to a positive integer: \"{}\"".format(Number))

    def _CheckBool(self, Value:bool):
        """ Check if value is boolean """
        if Value == 0 or Value == 1:
            return Value
        else:
            raise TypeError("The following value is not boolean: \"{}\"".format(Value))

    def ParseArguments(self, ArgumentsList:list=None):
        """ Parse script arguments """

        # Initialize parser
        Parser = ArgumentParser(
            prog="vic.py",
            description="Check video files integrity using ffmpeg.",
            usage="""vic.py ffmpeg_path [-h] [-q] [-t THREADS] [-ft FFMPEG_THREADS]
                  [-o OUTPUT] [-d DIRECTORY [DIRECTORY ...]] [-f FILE [FILE ...]]
                  [-x EXCLUDE [EXCLUDE ...]]""")

        # Add arguments to parse
        Parser.add_argument("-q", "--quiet", help="Suppress output", action="store_true", default=False)
        Parser.add_argument("-t", "--threads", help="Number of threads to use (default: 2)", type=self._CheckPositiveInteger, default=2)
        Parser.add_argument("-ft", "--ffmpeg_threads", help="Number of threads to use with ffmpeg (default: 2)", type=self._CheckPositiveInteger, default=2)
        Parser.add_argument("-o", "--output", help="Write output to a specified file", type=self._CheckFileWriteAccess, default="vic.log")
        Parser.add_argument("-d", "--directory", help="Directory where video files are stored", nargs="+", type=self._CheckFolderExistence)
        Parser.add_argument("-f", "--file", help="Video files to analyze", nargs="+", type=self._CheckFileExistence)
        Parser.add_argument("-x", "--exclude", help="Folders or files to exclude (absolute paths, regex allowed)", nargs="+", type=str)
        Parser.add_argument("ffmpeg_path", help="Path to ffmpeg binary", type=self._CheckFileExistence)

        # Parse arguments
        if isinstance(ArgumentsList, list):
            Arguments = Parser.parse_args(ArgumentsList)
        else:
            Arguments = Parser.parse_args()

        # Check for "directory" or "file" argument
        if Arguments.directory is None and Arguments.file is None:
            Parser.error("Either the \"--directory\" or \"--file\" parameter must be submitted.")

        return Arguments


class AnalyzeFiles():
    """ Class to analyze files with ffmpeg in threads """
    def __init__(self, FFmpegPath:str, Files:list, LogOutput:str=None, Threads:int=2, FFThreads:int=2, Quiet:bool=False):
        # Parse and test arguments
        MyArgumentParser = CustomArgumentParser()
        MyArgumentParser._CheckFileExistence(FFmpegPath)
        for File in Files: MyArgumentParser._CheckFileExistence(File)
        MyArgumentParser._CheckFileWriteAccess(LogOutput)
        MyArgumentParser._CheckPositiveInteger(Threads)
        MyArgumentParser._CheckPositiveInteger(FFThreads)
        MyArgumentParser._CheckBool(Quiet)

        # Set properties
        vars(self).update(locals())

        # Initialize lock for concurrent threads
        self.Lock = Lock()

        # Initialize Colorama
        ColoramaInit(autoreset=True)

        return

    def AnalyzeFile(self, Path:str):
        """ Analyse a single file with ffmpeg """

        # Check file existence
        if not os.path.isfile(Path):
            raise FileNotFoundError("The following file could not be found: \"{}\"".format(Path))

        # Execute ffmpeg process and get output
        FFmpegCommandArgs = [self.FFmpegPath, "-v", "error", "-i", Path, "-f", "null", "-threads", str(self.FFThreads), "-"]
        Process = Popen(FFmpegCommandArgs, stdout=PIPE, stderr=PIPE)
        ProcessResult = Process.communicate()[1]
        
        # Return if file is valid (if output is empty, then file is OK, else file is KO)
        return True if ProcessResult == b'' else False

    def Analyze(self):
        """ Analyze all files in parallel threads """

        # Create a list to store futures, mappings (to retrieve arguments) and results
        Futures = []
        FuturesMapping = {}
        FuturesResults = {}

        # Use ThreadPoolExecutor to launch multiple threads
        with ThreadPoolExecutor(max_workers=self.Threads) as Executor:
            # Try except to end all futures processes if Exception
            try:
                # Launch threads and set mapping
                for File in self.Files:
                    Future = Executor.submit(self.AnalyzeFile, File)
                    FuturesMapping[Future] = {"Path": File}
                    Futures.append(Future)

                # Get thread results
                for Future in CompletedFutures(Futures):
                    # Acquire the lock to prevent concurrent display
                    self.Lock.acquire()
                    # Process result
                    FuturesResults[FuturesMapping[Future]["Path"]] = True if Future.result() else False
                    # Process text result
                    Result = "{} \"{}\"".format("[OK]" if Future.result() else "[KO]", FuturesMapping[Future]["Path"])
                    # Print result (if not quiet)
                    if not self.Quiet:
                        print("{}{}{}".format(Fore.GREEN if Future.result() else Fore.RED, Style.BRIGHT, Result))
                    # Write output file (if set)
                    if self.LogOutput is not None:
                        try:
                            with open(self.LogOutput, "a", encoding="utf-8") as FDOutput:
                                FDOutput.write("{}\n".format(Result))
                        except OSError as e:
                            print("{}{}[WARNING] Could not write to file: {}".format(Fore.YELLOW, Style.BRIGHT, e))
                    # Release the lock
                    self.Lock.release()
            except KeyboardInterrupt:
                # Inform user that we are waiting for current thread
                print("Waiting for current thread to finish, press CTRL + C to force it to stop.")

                # Release lock if acquired
                if self.Lock.locked(): self.Lock.release()

                # Cancel threads in queue and shutdown current thread
                FutureThread._threads_queues.clear()
                Executor._threads.clear()
                Executor.shutdown(wait=False, cancel_futures=True)

        return FuturesResults


def main():
    # Parse arguments
    MyArgumentParser = CustomArgumentParser()
    Arguments = MyArgumentParser.ParseArguments()

    # Print banner and messages (if not quiet)
    if not Arguments.quiet:
        Banner = [
            "",
            "██╗   ██╗    ██╗     ██████╗",
            "██║   ██║    ██║    ██╔════╝",
            "██║   ██║    ██║    ██║     ",
            "╚██╗ ██╔╝    ██║    ██║     ",
            " ╚████╔╝     ██║    ╚██████╗",
            "  ╚═══╝      ╚═╝     ╚═════╝",
            "\nVideo Integrity Checker (VIC) v2.0.0 by Adrien CL",
            "Project under MIT License, check out \"license.txt\" to know more.\n"
        ]
        for line in Banner:
            print(line)

        if Arguments.threads > 1:
            print("{}{}[WARNING] This script uses a thread pool, therefore results might not be sorted as expected.\n".format(Fore.YELLOW, Style.BRIGHT))

        print("Processing files ...")

    # Write log separator
    if Arguments.output is not None:
        with open(Arguments.output, "a", encoding="utf-8") as FDOutput:
            FDOutput.write("==========\n")


    # Initialize file list and allowed extensions (video file extensions)
    FileList = []
    AllowedExtensions = ["mp4", "mkv", "avi", "mov", "wmv", "flv", "webm", "m4v"]

    # Process folders
    if Arguments.directory is not None:
        for folder in Arguments.directory:
            for (path, subfolders, subfiles) in os.walk(folder):
                for subfile in subfiles:
                    if re.search(r'.*\.({})$'.format('|'.join(AllowedExtensions)), subfile):
                        FileList.append("{}/{}".format(path, subfile).replace("\\", "/"))

    # Process files
    if Arguments.file is not None:
        FileList.extend([x.replace("\\", "/") for x in Arguments.file if re.search(r'.*\.({})$'.format('|'.join(AllowedExtensions)), x)])

    # Process exclusions:
    if Arguments.exclude is not None:
        try:
            FileList = [x for x in FileList if not re.search(r'({})'.format('|'.join(Arguments.exclude)), x)]
        except re.error as e:
            raise ValueError("Invalid regular expression: {}".format(e))


    # Analyze files in the list
    Analysis = AnalyzeFiles(Arguments.ffmpeg_path, FileList, Arguments.output, Arguments.threads, Arguments.ffmpeg_threads, Arguments.quiet)
    Results = Analysis.Analyze()


    # Count valid and invalid files
    Count = {"valid": 0, "invalid": 0}
    for x in Results:
        if Results[x] == 1: Count["valid"] += 1
        if Results[x] == 0: Count["invalid"] += 1

    # Print result number
    if not Arguments.quiet:
        print("Total files: {}".format(len(Results)))
        print("Total valid files: {}/{}\n".format(Count["valid"], len(Results)))
        print("Total invalid files: {}/{}\n".format(Count["invalid"], len(Results)))

    # Write result number
    if Arguments.output is not None:
        with open(Arguments.output, "a", encoding="utf-8") as FDOutput:
            FDOutput.write("Total files: {}\n".format(len(Results)))
            FDOutput.write("Total valid files: {}/{}\n".format(Count["valid"], len(Results)))
            FDOutput.write("Total invalid files: {}/{}\n".format(Count["invalid"], len(Results)))

    return


if __name__ == "__main__":
    main()